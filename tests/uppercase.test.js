const uppercase = require('../src/uppercase')

test('adalah become ADALAH',()=>{
    expect(uppercase('adalah')).toBe('ADALAH')
})

test('ialah doesnt supposed to be Ialah',()=>{
    expect(uppercase('ialah')).not.toBe('Ialah')
})