const power = require('../src/Power.js')

test('2^2 = 4', () => {
  expect(power(2, 2)).toBe(4)
})
